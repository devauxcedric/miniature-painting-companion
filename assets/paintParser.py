#!/usr/bin/python3

from bs4 import BeautifulSoup
from urllib.request import urlopen
import sys

html_soup = BeautifulSoup(sys.stdin,features="html5lib")

for paint_listing in html_soup.find_all(id="product-listing"):
    for day_activity in paint_listing.find_all("li"):
        found = day_activity.find_all("span")[0].find_all("img")[0]
        print(sys.argv[1] + "," + found["alt"] + "," + found["src"])

