# Miniature painting companion
A painting utility for mini painters with an android phone. Made with flutter.

I will help you keep a note a your different paint recipies with photos of their usages. Allows creatings custom paints mix.

Currently only paints from Citadel are managed but custom paints can be added manually (without photo)


# Screenshots 
<img src="screenshots/main.png" width="26%" height="26%" alt="Main screen" />
<img src="screenshots/paints.png" width="26%" height="26%" alt="Paints screen" />

<img src="screenshots/layers.png" width="26%" height="26%" alt="Layers screen" />
<img src="screenshots/note.png" width="26%" height="26%" alt="Note screen" />

# Building 
You must have flutter installed (follow this link for that: https://docs.flutter.dev/get-started/install)
Then import the project in android studio (not tested with other IDE) and everything should work as-is
