extension StringX on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }

  String camelToSentence() {
    var result = replaceAll(RegExp(r'(?<!^)(?=[A-Z])'), r" ");
    return result[0].toUpperCase() + result.substring(1);
  }
}
