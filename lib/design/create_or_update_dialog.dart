import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class CreateOrUpdateDialog {
  static show(
      context,
      Object? toUpdate,
      TextEditingController nameController,
      List<Widget> content,
      String createString,
      String updateString,
      Function(BuildContext, TextEditingValue, TextEditingController) onSave,
      Function(BuildContext, TextEditingValue, TextEditingController)
          onUpdate) {
    Alert(
        // removes the default theme of the extension which doesn't handle dark theme
        style: AlertStyle(
          titleStyle: TextStyle(fontFamily: "Gothic"),
          titlePadding: EdgeInsets.only(bottom: 24),
          alertPadding: EdgeInsets.only(left: 16, right: 16),
        ),
        context: context,
        title: (toUpdate == null) ? createString : updateString,
        content: Column(
          children: List.from(content)
            ..addAll([
              ValueListenableBuilder<TextEditingValue>(
                valueListenable: nameController,
                builder: (context, value, child) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 24),
                    child: FilledButton(
                      onPressed: nameController.text.isEmpty
                          ? null
                          : () => {
                                if (toUpdate != null)
                                  {onUpdate(context, value, nameController)}
                                else
                                  {onSave(context, value, nameController)},
                                Navigator.pop(context)
                              },
                      child: Text(
                        toUpdate == null
                            ? AppLocalizations.of(context)!.create
                            : AppLocalizations.of(context)!.update,
                        style: const TextStyle(fontSize: 20),
                      ),
                    ),
                  );
                },
              ),
            ]),
        ),
        buttons: []).show();
  }
}
