import 'dart:io';

import 'package:flutter/material.dart';
import 'package:miniature_painting_companion/utils.dart';

class UpdatableImage extends StatefulWidget {
  final String? initialValue;

  const UpdatableImage({super.key, this.initialValue});

  @override
  State<StatefulWidget> createState() {
    return UpdatableImageState();
  }
}

class UpdatableImageState extends State<UpdatableImage> {
  String? imagePath;

  void changeImage(String? imagePath) {
    setState(() {
      this.imagePath = imagePath;
    });
  }

  @override
  void initState() {
    super.initState();
    imagePath = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    if (imagePath != null) {
      return Image.file(fit: BoxFit.cover, File(imagePath!));
    }

    return Utils.getSvgIcon("placeholderFilled");
  }
}
