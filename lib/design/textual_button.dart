import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TextualButton extends StatelessWidget {
  final void Function() onPressed;
  final String? text;

  // TODO should create an enum with all icons somewhere
  final String iconName;

  const TextualButton(
      {super.key, required this.onPressed, this.text, required this.iconName});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton.outlined(
          style: OutlinedButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadiusDirectional.all(Radius.circular(8))),
              backgroundColor: Colors.blueGrey),
          icon: SvgPicture.asset(
              colorFilter: ColorFilter.mode(Colors.white, BlendMode.srcIn),
              width: 40,
              height: 40,
              "assets/$iconName.svg"),
          iconSize: 40,
          tooltip: text,
          onPressed: onPressed,
        ),
        if (text != null)
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: Text.rich(
              style: const TextStyle(fontSize: 12, fontFamily: "Gothic"),
              TextSpan(text: text),
              textAlign: TextAlign.center,
            ),
          )
      ],
    );
  }
}
