import 'package:flutter/material.dart';

class NamedSlider extends StatefulWidget {
  final ValueNotifier<int> notifier;

  const NamedSlider({super.key, required this.notifier});

  @override
  State<NamedSlider> createState() => _NamedSliderState();
}

class _NamedSliderState extends State<NamedSlider> {
  @override
  Widget build(BuildContext context) {
    return Slider(
      min: 0,
      max: 100,
      divisions: 10,
      value: widget.notifier.value.toDouble(),
      label: widget.notifier.value.round().toString(),
      onChanged: (double value) {
        setState(() {
          widget.notifier.value = value.toInt();
        });
      },
    );
  }
}
