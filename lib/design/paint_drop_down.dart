import 'package:flutter/material.dart';
import 'package:miniature_painting_companion/extensions/string_extension.dart';

import '../Models/hive_models.dart';

class PaintDropdown extends StatefulWidget {
  final ValueNotifier<ApplicationType> notifier;

  const PaintDropdown(this.notifier, {super.key});

  @override
  State<PaintDropdown> createState() => _PaintDropdownState();
}

class _PaintDropdownState extends State<PaintDropdown> {
  _PaintDropdownState();

  ApplicationType dropdownValue = ApplicationType.normal;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<ApplicationType>(
      value: dropdownValue,
      underline: Container(
        height: 2,
      ),
      onChanged: (ApplicationType? newValue) {
        setState(() {
          dropdownValue = newValue!;
          widget.notifier.value = newValue;
        });
      },
      items: ApplicationType.values
          .map((e) => DropdownMenuItem<ApplicationType>(
              value: e, child: Text(e.name.camelToSentence().capitalize())))
          .toList(),
    );
  }
}
