import 'package:flutter/material.dart';

class CustomPopUpMenu extends StatelessWidget {
  final List<({String key, String value, void Function() action})> items;
  final bool displayOptionals;
  final Widget? child;

  const CustomPopUpMenu(
      {super.key,
      required this.items,
      required this.displayOptionals,
      this.child});

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      onSelected: (String result) {
        items.firstWhere((elem) => elem.key == result).action();
      },
      itemBuilder: (BuildContext context) => items
          .map((elem) => PopupMenuItem<String>(
                value: elem.key,
                child: Text(elem.value),
              ))
          .where((elem) => displayOptionals || !elem.value!.startsWith('?'))
          .toList(),
      child: child,
    );
  }
}
