import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_picker/image_picker.dart';
import 'package:miniature_painting_companion/design/textual_button.dart';
import 'package:miniature_painting_companion/design/updatable_image.dart';

import '../utils.dart';

class ImagePickingWidget extends StatelessWidget {
  final GlobalKey<UpdatableImageState> imageKey;
  final String? initialValue;

  const ImagePickingWidget(
      {super.key, required this.imageKey, required this.initialValue});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: TextualButton(
                    onPressed: () async {
                      imageKey.currentState?.changeImage(
                          (await Utils.pickImage(ImageSource.gallery))?.path);
                    },
                    text: AppLocalizations.of(context)!.choosePicture,
                    iconName: "upload_file"),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: TextualButton(
                    onPressed: () async {
                      imageKey.currentState?.changeImage(
                          (await Utils.pickImage(ImageSource.camera))?.path);
                    },
                    text: AppLocalizations.of(context)!.takePicture,
                    iconName: "take_picture"),
              ),
            )
          ],
        ),
        SizedBox(
          height: 16,
        ),
        IconButton(
          icon: ClipOval(
            child: SizedBox.fromSize(
              size: Size.fromRadius(64),
              child: UpdatableImage(
                key: imageKey,
                initialValue: initialValue,
              ),
            ),
          ),
          tooltip: AppLocalizations.of(context)!.takePicture,
          onPressed: () async {
            if (imageKey.currentState?.imagePath == null) {
              imageKey.currentState?.changeImage(
                  (await Utils.pickImage(ImageSource.camera))?.path);
            } else {
              Utils.showImageEditor(imageKey.currentState!.imagePath!, context);
            }
          },
        ),
        SizedBox(
          height: 16,
        )
      ],
    );
  }
}
