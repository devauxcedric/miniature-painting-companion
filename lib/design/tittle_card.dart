import 'package:flutter/material.dart';
import 'package:miniature_painting_companion/design/custom_pop_up_menu.dart';
import 'package:miniature_painting_companion/utils.dart';

class TittleCard extends StatelessWidget {
  final void Function() onTap;
  final String? imagePath;
  final String title;
  final CustomPopUpMenu popUp;

  const TittleCard(
      {super.key,
      required this.onTap,
      this.imagePath,
      required this.title,
      required this.popUp});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: InkWell(
          onTap: () {
            onTap();
          },
          child: Column(
            children: [
              Expanded(flex: 4, child: Utils.getImageOrPlaceholder(imagePath)),
              Expanded(
                child: Row(children: [
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: Center(
                        child: Text(title,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(fontFamily: 'Gothic'))),
                  ),
                  popUp
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
