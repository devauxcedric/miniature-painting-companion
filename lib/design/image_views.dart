import 'dart:io';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class ImageViews extends StatelessWidget {
  final List<String> path;
  final int? initialPage;
  final String? name;

  const ImageViews(this.path, {super.key, this.initialPage, this.name});

  @override
  Widget build(
    BuildContext context,
  ) {
    //var file = FileImage(File(path));

    return Scaffold(
      appBar: AppBar(
        title: Text("title"),
      ),
      body: PhotoViewGallery.builder(
        backgroundDecoration: BoxDecoration(color: Colors.transparent),
        scrollPhysics: const BouncingScrollPhysics(),
        builder: (BuildContext context, int index) {
          return PhotoViewGalleryPageOptions(
            imageProvider: FileImage(File(path[index])),
            initialScale: PhotoViewComputedScale.contained,
            minScale: PhotoViewComputedScale.contained,
            maxScale: PhotoViewComputedScale.covered * 20,
            heroAttributes: PhotoViewHeroAttributes(
                transitionOnUserGestures: true, tag: path[index]),
          );
        },
        pageController: PageController(initialPage: initialPage ?? 0),
        itemCount: path.length,
        loadingBuilder: (context, progress) => Center(
          child: SizedBox(
            width: 20.0,
            height: 20.0,
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
