// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'export_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MiniatureHolder _$MiniatureHolderFromJson(Map<String, dynamic> json) =>
    MiniatureHolder(
      (json['miniatures'] as List<dynamic>)
          .map((e) => MiniatureExport.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MiniatureHolderToJson(MiniatureHolder instance) =>
    <String, dynamic>{
      'miniatures': instance.miniatures.map((e) => e.toJson()).toList(),
    };

MiniatureExport _$MiniatureExportFromJson(Map<String, dynamic> json) =>
    MiniatureExport(
      json['name'] as String,
      json['army'] as String,
      json['imagePath'] as String?,
      json['imageBase64'] as String?,
      (json['paintJobs'] as List<dynamic>?)
          ?.map((e) => PaintJobExport.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['historyImages'] as List<dynamic>?)
          ?.map((e) => HistoryImageExport.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MiniatureExportToJson(MiniatureExport instance) =>
    <String, dynamic>{
      'name': instance.name,
      'army': instance.army,
      'imagePath': instance.imagePath,
      'imageBase64': instance.imageBase64,
      'paintJobs': instance.paintJobs?.map((e) => e.toJson()).toList(),
      'historyImages': instance.historyImages?.map((e) => e.toJson()).toList(),
    };

PaintJobExport _$PaintJobExportFromJson(Map<String, dynamic> json) =>
    PaintJobExport(
      json['imagePath'] as String?,
      json['name'] as String,
      (json['separatorLayers'] as List<dynamic>?)
          ?.map((e) => LayerSeparatorExport.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['paintLayers'] as List<dynamic>?)
          ?.map((e) => PaintLayerExport.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['combinedLayers'] as List<dynamic>?)
          ?.map((e) => CombinedLayerExport.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['stickerLayers'] as List<dynamic>?)
          ?.map((e) => StickerLayerExport.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PaintJobExportToJson(PaintJobExport instance) =>
    <String, dynamic>{
      'imagePath': instance.imagePath,
      'name': instance.name,
      'separatorLayers':
          instance.separatorLayers?.map((e) => e.toJson()).toList(),
      'paintLayers': instance.paintLayers?.map((e) => e.toJson()).toList(),
      'combinedLayers':
          instance.combinedLayers?.map((e) => e.toJson()).toList(),
      'stickerLayers': instance.stickerLayers?.map((e) => e.toJson()).toList(),
    };

PaintLayerExport _$PaintLayerExportFromJson(Map<String, dynamic> json) =>
    PaintLayerExport(
      (json['position'] as num).toInt(),
      json['name'] as String,
      Paint.fromJson(json['paint'] as Map<String, dynamic>),
      json['note'] as String,
      $enumDecode(_$ApplicationTypeEnumMap, json['applicationType']),
      (json['dilution'] as num).toInt(),
    );

Map<String, dynamic> _$PaintLayerExportToJson(PaintLayerExport instance) =>
    <String, dynamic>{
      'position': instance.position,
      'name': instance.name,
      'paint': instance.paint.toJson(),
      'note': instance.note,
      'applicationType': _$ApplicationTypeEnumMap[instance.applicationType]!,
      'dilution': instance.dilution,
    };

const _$ApplicationTypeEnumMap = {
  ApplicationType.normal: 'normal',
  ApplicationType.dryBrush: 'dryBrush',
  ApplicationType.lacy: 'lacy',
};

LayerSeparatorExport _$LayerSeparatorExportFromJson(
        Map<String, dynamic> json) =>
    LayerSeparatorExport(
      (json['position'] as num).toInt(),
      json['text'] as String,
    );

Map<String, dynamic> _$LayerSeparatorExportToJson(
        LayerSeparatorExport instance) =>
    <String, dynamic>{
      'position': instance.position,
      'text': instance.text,
    };

CombinedLayerExport _$CombinedLayerExportFromJson(Map<String, dynamic> json) =>
    CombinedLayerExport(
      (json['position'] as num).toInt(),
      json['name'] as String,
      (json['separatorLayers'] as List<dynamic>?)
          ?.map((e) => LayerSeparatorExport.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['paintLayers'] as List<dynamic>?)
          ?.map((e) => PaintLayerExport.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['combinedLayers'] as List<dynamic>?)
          ?.map((e) => CombinedLayerExport.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CombinedLayerExportToJson(
        CombinedLayerExport instance) =>
    <String, dynamic>{
      'position': instance.position,
      'name': instance.name,
      'separatorLayers':
          instance.separatorLayers?.map((e) => e.toJson()).toList(),
      'paintLayers': instance.paintLayers?.map((e) => e.toJson()).toList(),
      'combinedLayers':
          instance.combinedLayers?.map((e) => e.toJson()).toList(),
    };

StickerLayerExport _$StickerLayerExportFromJson(Map<String, dynamic> json) =>
    StickerLayerExport(
      (json['position'] as num).toInt(),
      json['name'] as String,
      json['imagePath'] as String?,
      json['imageBase64'] as String?,
    );

Map<String, dynamic> _$StickerLayerExportToJson(StickerLayerExport instance) =>
    <String, dynamic>{
      'position': instance.position,
      'name': instance.name,
      'imagePath': instance.imagePath,
      'imageBase64': instance.imageBase64,
    };

HistoryImageExport _$HistoryImageExportFromJson(Map<String, dynamic> json) =>
    HistoryImageExport(
      json['imagePath'] as String,
      json['imageBase64'] as String,
      DateTime.parse(json['importDate'] as String),
    );

Map<String, dynamic> _$HistoryImageExportToJson(HistoryImageExport instance) =>
    <String, dynamic>{
      'imagePath': instance.imagePath,
      'imageBase64': instance.imageBase64,
      'importDate': instance.importDate.toIso8601String(),
    };
