import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

abstract class Cloneable<T> {
  T clone();
}

abstract class Exportable<T, S> {
  S toExport();
}

abstract class OrderedExportable<T, S> {
  S toExport(int position);
}

abstract class IDrawableLayer extends HiveObject
    implements Cloneable, OrderedExportable {
  Widget getWidget(Key? key);
}
