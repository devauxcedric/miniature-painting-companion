import 'dart:convert';
import 'dart:io';

import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:miniature_painting_companion/Models/model_interfaces.dart';
import 'package:miniature_painting_companion/main.dart';

import 'export_models.dart';
import 'layers_model.dart';

part 'hive_models.g.dart';

// TODO the whole import/export part is a mess and must be checked thoroughly
// TODO function to add or delete child entities
@HiveType(typeId: 0)
class Miniature extends HiveObject implements Cloneable, Exportable {
  @HiveField(0)
  String name;

  @HiveField(1)
  String army;

  @HiveField(2)
  String? imagePath;

  @HiveField(3)
  HiveList paintJobs;

  @HiveField(4)
  HiveList historyImages;

  Miniature(
      this.name, this.army, this.imagePath, this.paintJobs, this.historyImages);

  @override
  Miniature clone() {
    var cloned = Miniature(name, army, imagePath, HiveList(paintJobBox),
        HiveList(historyImagesBox));

    paintJobs.cast<PaintJob>().forEach((paintJob) {
      cloned.paintJobs.add(paintJob.clone());
    });

    historyImages.cast<HistoryImage>().forEach((image) {
      cloned.historyImages.add(image.clone());
    });

    miniaturesBox.add(cloned);
    return cloned;
  }

  @override
  MiniatureExport toExport() {
    var paintJobs =
        this.paintJobs.cast<PaintJob>().map((e) => e.toExport()).toList();

    var historyImages = this
        .historyImages
        .cast<HistoryImage>()
        .map((e) => e.toExport())
        .toList();

    String? imageBase64;
    if (imagePath != null) {
      File file = File(imagePath!);
      List<int> fileInByte = file.readAsBytesSync();
      imageBase64 = base64Encode(fileInByte);
    }

    return MiniatureExport(
        name, army, imagePath, imageBase64, paintJobs, historyImages);
  }

  static import(MiniatureExport export) {
    if (export.imagePath != null) {
      File(export.imagePath!).createSync(recursive: true);
      File image = File(export.imagePath!);
      image.writeAsBytes(base64Decode(export.imageBase64!));
    }

    var created = Miniature(export.name, export.army, export.imagePath,
        HiveList(paintJobBox), HiveList(historyImagesBox));
    miniaturesBox.add(created);

    export.paintJobs?.forEach((element) {
      PaintJob.import(element, created);
    });
    export.historyImages?.forEach((element) {
      HistoryImage.import(element, created);
    });
  }
}

class MiniatureAdapter extends TypeAdapter<Miniature> {
  @override
  final typeId = 0;

  @override
  Miniature read(BinaryReader reader) {
    var miniature = Miniature(
        reader.read(),
        reader.read(),
        reader.read(),
        reader.read() ?? HiveList(paintJobBox),
        reader.availableBytes > 0
            ? reader.read() ?? HiveList(historyImagesBox)
            : HiveList(historyImagesBox));

    return miniature;
  }

  @override
  void write(BinaryWriter writer, Miniature obj) {
    writer.write(obj.name);
    writer.write(obj.army);
    writer.write(obj.imagePath);
    writer.write(obj.paintJobs);
    writer.write(obj.historyImages);
  }
}

@HiveType(typeId: 1)
class PaintJob extends HiveObject implements Cloneable, Exportable {
  @HiveField(0)
  String? imagePath;

  @HiveField(1)
  String name;

  @HiveField(2)
  HiveList paintLayers;

  PaintJob({this.imagePath, required this.name, required this.paintLayers});

  @override
  PaintJob clone() {
    var cloned = PaintJob(
        imagePath: imagePath, name: name, paintLayers: HiveList(layersBox));

    for (IDrawableLayer layer in paintLayers.cast<IDrawableLayer>()) {
      cloned.paintLayers.add(layer.clone());
    }

    paintJobBox.add(cloned);
    return cloned;
  }

  @override
  PaintJobExport toExport() {
    List<LayerSeparatorExport> separatorLayers = [];
    List<PaintLayerExport> paintLayers = [];
    List<CombinedLayerExport> combinedLayers = [];
    List<StickerLayerExport> stickerLayers = [];

    var casted = this.paintLayers.cast();

    for (int i = 0; i < casted.length; i++) {
      var layerAct = casted[i];
      if (layerAct is LayerSeparator) {
        separatorLayers.add(layerAct.toExport(i));
      } else if (layerAct is PaintLayer) {
        paintLayers.add(layerAct.toExport(i));
      } else if (layerAct is CombinedLayer) {
        combinedLayers.add(layerAct.toExport(i));
      } else if (layerAct is StickerLayer) {
        stickerLayers.add(layerAct.toExport(i));
      } else {
        throw FormatException("this type of layer isn't supported: $layerAct");
      }
    }

    return PaintJobExport(imagePath, name, separatorLayers, paintLayers,
        combinedLayers, stickerLayers);
  }

  static import(PaintJobExport export, Miniature holder) {
    var created = PaintJob(
        imagePath: export.imagePath,
        name: export.name,
        paintLayers: HiveList(layersBox));
    paintJobBox.add(created);

    Map<int, IDrawableLayer> importedLayers = {};

    export.paintLayers?.forEach((element) {
      importedLayers.putIfAbsent(
          element.position, () => PaintLayer.fromExport(element));
    });
    export.combinedLayers?.forEach((element) {
      importedLayers.putIfAbsent(
          element.position, () => CombinedLayer.fromExport(element, created));
    });
    export.separatorLayers?.forEach((element) {
      importedLayers.putIfAbsent(
          element.position, () => LayerSeparator.fromExport(element));
    });
    export.stickerLayers?.forEach((element) {
      importedLayers.putIfAbsent(
          element.position, () => StickerLayer.fromExport(element));
    });

    var sorted = importedLayers.entries.toList();
    sorted.sort((a, b) => a.key.compareTo(b.key));
    var mapped = sorted.map((e) => e.value).toList();

    for (var element in mapped) {
      created.paintLayers.add(element);
    }
    created.save();

    holder.paintJobs.add(created);
    holder.save();
  }
}

class PaintJobAdapter extends TypeAdapter<PaintJob> {
  @override
  final typeId = 1;

  @override
  PaintJob read(BinaryReader reader) {
    return PaintJob(
        imagePath: reader.read(),
        name: reader.read(),
        paintLayers: reader.read() ?? HiveList(layersBox));
  }

  @override
  void write(BinaryWriter writer, PaintJob obj) {
    writer.write(obj.imagePath);
    writer.write(obj.name);
    writer.write(obj.paintLayers);
  }
}

@HiveType(typeId: 2)
enum ApplicationType {
  @HiveField(0)
  normal,
  @HiveField(1)
  dryBrush,
  @HiveField(2)
  lacy
}

class ApplicationTypeAdapter extends TypeAdapter<ApplicationType> {
  @override
  final typeId = 2;

  @override
  ApplicationType read(BinaryReader reader) {
    return ApplicationType.normal;
  }

  @override
  void write(BinaryWriter writer, ApplicationType obj) {
    writer.write(obj.index);
  }
}

// typeId: 3 and 4 is for layers

@HiveType(typeId: 5)
@JsonSerializable()
class Paint extends HiveObject {
  @HiveField(0)
  String type;

  @HiveField(1)
  String name;

  @HiveField(2)
  String? image;

  Paint(this.type, this.name, this.image);

  factory Paint.fromJson(Map<String, dynamic> json) => _$PaintFromJson(json);

  Map<String, dynamic> toJson() => _$PaintToJson(this);
}

class PaintAdapter extends TypeAdapter<Paint> {
  @override
  final typeId = 5;

  @override
  Paint read(BinaryReader reader) {
    return Paint(reader.read(), reader.read(), reader.read());
  }

  @override
  void write(BinaryWriter writer, Paint obj) {
    writer.write(obj.type);
    writer.write(obj.name);
    writer.write(obj.image);
  }
}

@HiveType(typeId: 8)
@JsonSerializable()
class HistoryImage extends HiveObject implements Cloneable, Exportable {
  @HiveField(0)
  String imagePath;

  @HiveField(1)
  DateTime importTime;

  HistoryImage(this.imagePath, this.importTime);

  @override
  HistoryImageExport toExport() {
    File file = File(imagePath);
    List<int> fileInByte = file.readAsBytesSync();
    String imageBase64 = base64Encode(fileInByte);

    return HistoryImageExport(imagePath, imageBase64, importTime);
  }

  static import(HistoryImageExport export, Miniature holder) {
    File(export.imagePath).createSync(recursive: true);
    File image = File(export.imagePath);
    image.writeAsBytes(base64Decode(export.imageBase64));

    var created = HistoryImage(export.imagePath, export.importDate);
    historyImagesBox.add(created);

    holder.historyImages.add(created);
    holder.save();
  }

  @override
  clone() {
    var cloned = HistoryImage(imagePath, importTime);
    historyImagesBox.add(cloned);
    return cloned;
  }
}

class HistoryImageAdapter extends TypeAdapter<HistoryImage> {
  @override
  final typeId = 8;

  @override
  HistoryImage read(BinaryReader reader) {
    return HistoryImage(reader.read(), reader.read());
  }

  @override
  void write(BinaryWriter writer, HistoryImage obj) {
    writer.write(obj.imagePath);
    writer.write(obj.importTime);
  }
}
