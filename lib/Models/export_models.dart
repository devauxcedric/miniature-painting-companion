import 'package:json_annotation/json_annotation.dart';

import 'hive_models.dart';

part 'export_models.g.dart';

// generate _$xxxto/fromJson with  flutter pub run build_runner build

// used to export data
@JsonSerializable()
class MiniatureHolder {
  List<MiniatureExport> miniatures;

  MiniatureHolder(this.miniatures);

  static fromBasicType(List<Miniature> mini) {
    return MiniatureHolder(mini.map((e) => e.toExport()).toList());
  }

  factory MiniatureHolder.fromJson(Map<String, dynamic> json) =>
      _$MiniatureHolderFromJson(json);

  Map<String, dynamic> toJson() => _$MiniatureHolderToJson(this);
}

@JsonSerializable()
class MiniatureExport {
  String name;
  String army;
  String? imagePath;
  String? imageBase64;
  List<PaintJobExport>? paintJobs;
  List<HistoryImageExport>? historyImages;

  MiniatureExport(this.name, this.army, this.imagePath, this.imageBase64,
      this.paintJobs, this.historyImages);

  factory MiniatureExport.fromJson(Map<String, dynamic> json) =>
      _$MiniatureExportFromJson(json);

  Map<String, dynamic> toJson() => _$MiniatureExportToJson(this);
}

@JsonSerializable()
class PaintJobExport {
  String? imagePath;
  String name;

  // TODO this is ugly and will need to be removed
  List<LayerSeparatorExport>? separatorLayers;
  List<PaintLayerExport>? paintLayers;
  List<CombinedLayerExport>? combinedLayers;
  List<StickerLayerExport>? stickerLayers;

  PaintJobExport(this.imagePath, this.name, this.separatorLayers,
      this.paintLayers, this.combinedLayers, this.stickerLayers);

  factory PaintJobExport.fromJson(Map<String, dynamic> json) =>
      _$PaintJobExportFromJson(json);

  Map<String, dynamic> toJson() => _$PaintJobExportToJson(this);
}

@JsonSerializable()
class PaintLayerExport {
  int position;

  String name;

  // TODO think we could juste export their ID
  Paint paint;
  String note;
  ApplicationType applicationType;
  int dilution;

  PaintLayerExport(this.position, this.name, this.paint, this.note,
      this.applicationType, this.dilution);

  factory PaintLayerExport.fromJson(Map<String, dynamic> json) =>
      _$PaintLayerExportFromJson(json);

  Map<String, dynamic> toJson() => _$PaintLayerExportToJson(this);
}

@JsonSerializable()
class LayerSeparatorExport {
  int position;

  String text;

  LayerSeparatorExport(this.position, this.text);

  factory LayerSeparatorExport.fromJson(Map<String, dynamic> json) =>
      _$LayerSeparatorExportFromJson(json);

  Map<String, dynamic> toJson() => _$LayerSeparatorExportToJson(this);
}

@JsonSerializable()
class CombinedLayerExport {
  int position;

  String name;

  // TODO this is ugly and will need to be removed
  List<LayerSeparatorExport>? separatorLayers;
  List<PaintLayerExport>? paintLayers;
  List<CombinedLayerExport>? combinedLayers;

  CombinedLayerExport(this.position, this.name, this.separatorLayers,
      this.paintLayers, this.combinedLayers);

  factory CombinedLayerExport.fromJson(Map<String, dynamic> json) =>
      _$CombinedLayerExportFromJson(json);

  Map<String, dynamic> toJson() => _$CombinedLayerExportToJson(this);
}

@JsonSerializable()
class StickerLayerExport {
  int position;

  String name;
  String? imagePath;
  String? imageBase64;

  StickerLayerExport(
      this.position, this.name, this.imagePath, this.imageBase64);

  factory StickerLayerExport.fromJson(Map<String, dynamic> json) =>
      _$StickerLayerExportFromJson(json);

  Map<String, dynamic> toJson() => _$StickerLayerExportToJson(this);
}

@JsonSerializable()
class HistoryImageExport {
  String imagePath;
  String imageBase64;
  DateTime importDate;

  HistoryImageExport(this.imagePath, this.imageBase64, this.importDate);

  factory HistoryImageExport.fromJson(Map<String, dynamic> json) =>
      _$HistoryImageExportFromJson(json);

  Map<String, dynamic> toJson() => _$HistoryImageExportToJson(this);
}
