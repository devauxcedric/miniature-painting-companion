// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hive_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Paint _$PaintFromJson(Map<String, dynamic> json) => Paint(
      json['type'] as String,
      json['name'] as String,
      json['image'] as String?,
    );

Map<String, dynamic> _$PaintToJson(Paint instance) => <String, dynamic>{
      'type': instance.type,
      'name': instance.name,
      'image': instance.image,
    };

HistoryImage _$HistoryImageFromJson(Map<String, dynamic> json) => HistoryImage(
      json['imagePath'] as String,
      DateTime.parse(json['importTime'] as String),
    );

Map<String, dynamic> _$HistoryImageToJson(HistoryImage instance) =>
    <String, dynamic>{
      'imagePath': instance.imagePath,
      'importTime': instance.importTime.toIso8601String(),
    };
