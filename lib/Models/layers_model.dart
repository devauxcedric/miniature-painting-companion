import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:miniature_painting_companion/Layers/combined/combined_layer_widget.dart';
import 'package:miniature_painting_companion/Layers/paint_layer/paint_layer_widget.dart';
import 'package:miniature_painting_companion/Layers/separator/separator_layer_widget.dart';
import 'package:miniature_painting_companion/Models/export_models.dart';
import 'package:miniature_painting_companion/Models/model_interfaces.dart';
import 'package:miniature_painting_companion/main.dart';

import '../Layers/sticker/sticker_layer_widget.dart';
import 'hive_models.dart';

// Paint Layer
@HiveType(typeId: 3)
class PaintLayer extends HiveObject implements IDrawableLayer {
  @HiveField(0)
  String name;

  @HiveField(1)
  Paint paint;

  @HiveField(2)
  String note;

  @HiveField(3)
  ApplicationType applicationType;

  @HiveField(4)
  int dilution;

  PaintLayer(
      this.name, this.paint, this.note, this.applicationType, this.dilution);

  @override
  Widget getWidget(Key? key) {
    return PaintLayerWidget(key: key, layer: this);
  }

  @override
  PaintLayer clone() {
    var cloned = PaintLayer(name, paint, note, applicationType, dilution);
    layersBox.add(cloned);
    return cloned;
  }

  @override
  PaintLayerExport toExport(int pos) {
    return PaintLayerExport(pos, name, paint, note, applicationType, dilution);
  }

  static fromExport(PaintLayerExport export) {
    var created = PaintLayer(export.name, export.paint, export.note,
        export.applicationType, export.dilution);
    layersBox.add(created);
    return created;
  }
}

class PaintLayerAdapter extends TypeAdapter<PaintLayer> {
  @override
  final typeId = 3;

  @override
  PaintLayer read(BinaryReader reader) {
    return PaintLayer(reader.read(), reader.read(), reader.read(),
        reader.read(), reader.readInt());
  }

  @override
  void write(BinaryWriter writer, PaintLayer obj) {
    writer.write(obj.name);
    writer.write(obj.paint);
    writer.write(obj.note);
    writer.write(obj.applicationType);
    writer.write(obj.dilution);
  }
}

// Layer separator
@HiveType(typeId: 4)
class LayerSeparator extends HiveObject implements IDrawableLayer {
  @HiveField(0)
  String text;

  LayerSeparator(this.text);

  @override
  Widget getWidget(Key? key) {
    return SeparatorLayerWidget(key: key, separator: this);
  }

  @override
  LayerSeparator clone() {
    var cloned = LayerSeparator(text);
    layersBox.add(cloned);
    return cloned;
  }

  @override
  LayerSeparatorExport toExport(int pos) {
    return LayerSeparatorExport(pos, text);
  }

  static fromExport(LayerSeparatorExport export) {
    var created = LayerSeparator(export.text);
    layersBox.add(created);
    return created;
  }
}

class LayerSeparatorAdapter extends TypeAdapter<LayerSeparator> {
  @override
  final typeId = 4;

  @override
  LayerSeparator read(BinaryReader reader) {
    return LayerSeparator(reader.read());
  }

  @override
  void write(BinaryWriter writer, LayerSeparator obj) {
    writer.write(obj.text);
  }
}

// Combined Layer
@HiveType(typeId: 6)
class CombinedLayer extends HiveObject implements IDrawableLayer {
  @HiveField(0)
  String name;

  // TODO for now this one keep being optional as its box is recursive and this cause issue at initialisation
  @HiveField(2)
  HiveList? layers;

  CombinedLayer({required this.name, required this.layers});

  @override
  Widget getWidget(Key? key) {
    return CombinedLayerWidget(
      key: key,
      layer: this,
    );
  }

  @override
  CombinedLayer clone() {
    var cloned = CombinedLayer(name: name, layers: HiveList(layersBox));

    for (PaintLayer layer in layers!.cast<PaintLayer>()) {
      cloned.layers!.add(layer.clone());
    }

    layersBox.add(cloned);
    return cloned;
  }

  @override
  CombinedLayerExport toExport(int pos) {
    //TODO ugly will need to remove this
    List<LayerSeparatorExport> separatorLayers = [];
    List<PaintLayerExport> paintLayers = [];
    List<CombinedLayerExport> combinedLayers = [];

    var casted = layers?.cast();

    if (casted != null) {
      for (int i = 0; i < casted.length; i++) {
        var layerAct = casted[i];
        if (layerAct is LayerSeparator) {
          separatorLayers.add(layerAct.toExport(i));
        } else if (layerAct is PaintLayer) {
          paintLayers.add(layerAct.toExport(i));
        } else if (layerAct is CombinedLayer) {
          combinedLayers.add(layerAct.toExport(i));
        } else {
          throw FormatException(
              "this type of layer isn't supported: $layerAct");
        }
      }
    }

    return CombinedLayerExport(
        pos, name, separatorLayers, paintLayers, combinedLayers);
  }

  static fromExport(CombinedLayerExport export, PaintJob holder) {
    var created = CombinedLayer(name: export.name, layers: HiveList(layersBox));
    layersBox.add(created);

    Map<int, IDrawableLayer> importedLayers = {};

    export.paintLayers?.forEach((element) {
      importedLayers.putIfAbsent(
          element.position, () => PaintLayer.fromExport(element));
    });

    var sorted = importedLayers.entries.toList();
    sorted.sort((a, b) => a.key.compareTo(b.key));
    var mapped = sorted.map((e) => e.value).toList();

    for (var element in mapped) {
      created.layers?.add(element);
    }
    created.save();

    return created;
  }
}

class CombinedLayerAdapter extends TypeAdapter<CombinedLayer> {
  @override
  final typeId = 6;

  @override
  CombinedLayer read(BinaryReader reader) {
    return CombinedLayer(name: reader.read(), layers: reader.read());
  }

  @override
  void write(BinaryWriter writer, CombinedLayer obj) {
    writer.write(obj.name);
    writer.write(obj.layers);
  }
}

// Sticker separator
@HiveType(typeId: 7)
class StickerLayer extends HiveObject implements IDrawableLayer {
  @HiveField(0)
  String name;

  @HiveField(1)
  String? imagePath;

  StickerLayer(this.name, this.imagePath);

  @override
  Widget getWidget(Key? key) {
    return StickerLayerWidget(key: key, sticker: this);
  }

  @override
  StickerLayer clone() {
    var cloned = StickerLayer(name, imagePath);
    layersBox.add(cloned);
    return cloned;
  }

  @override
  StickerLayerExport toExport(int pos) {
    String? imageBase64;
    if (imagePath != null) {
      File file = File(imagePath!);
      List<int> fileInByte = file.readAsBytesSync();
      imageBase64 = base64Encode(fileInByte);
    }

    return StickerLayerExport(pos, name, imagePath, imageBase64);
  }

  static fromExport(StickerLayerExport export) {
    if (export.imagePath != null) {
      File(export.imagePath!).createSync(recursive: true);
      File image = File(export.imagePath!);
      image.writeAsBytes(base64Decode(export.imageBase64!));
    }

    var created = StickerLayer(export.name, export.imagePath);
    layersBox.add(created);
    return created;
  }
}

class StickerLayerAdapter extends TypeAdapter<StickerLayer> {
  @override
  final typeId = 7;

  @override
  StickerLayer read(BinaryReader reader) {
    return StickerLayer(reader.read(), reader.read());
  }

  @override
  void write(BinaryWriter writer, StickerLayer obj) {
    writer.write(obj.name);
    writer.write(obj.imagePath);
  }
}
