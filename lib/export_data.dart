import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


import 'Models/export_models.dart';
import 'Models/hive_models.dart';
import 'main.dart';

class DataExporter {
  static void _checkStoragePermission(dynamic context) async {
    var status = await Permission.manageExternalStorage.status;
    if (status.isRestricted) {
      status = await Permission.manageExternalStorage.request();
    }

    if (status.isDenied) {
      status = await Permission.manageExternalStorage.request();
    }
    if (status.isPermanentlyDenied) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.green,
        content: Text(AppLocalizations.of(context)!.accessExternalStorageRequired),
      ));
    }
  }

  static void exportData(context) async {
    _checkStoragePermission(context);

    var destination = await FilePicker.platform.getDirectoryPath();
    if (destination != null) {
      var mini = miniaturesBox.values.cast<Miniature>().toList();
      var holder = MiniatureHolder.fromBasicType(mini).toJson();
      var formattedTime =
          DateFormat("yyyy_MM_dd_hh_mm_ss").format(DateTime.now());
      final File file =
          File("$destination/miniature_companion_export$formattedTime.json");
      await file.writeAsString(jsonEncode(holder));
    }
  }

  static void importData(context) async {
    _checkStoragePermission(context);
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      File file = File(result.files.single.path!);
      var imported =
          MiniatureHolder.fromJson(jsonDecode(file.readAsStringSync()));

      for (var element in imported.miniatures) {
        Miniature.import(element);
      }
    }
  }
}
