import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../Models/model_interfaces.dart';
import '../main.dart';

class LayersList extends StatelessWidget {
  final List<IDrawableLayer> layers;
  final bool nested;

  const LayersList({super.key, required this.layers, required this.nested});

  @override
  Widget build(BuildContext context) {
    Widget proxyDecorator(
        Widget child, int index, Animation<double> animation) {
      return AnimatedBuilder(
        animation: animation,
        builder: (BuildContext context, Widget? child) {
          return Material(
            elevation: 16,
            color: Colors.transparent,
            child: child,
          );
        },
        child: child,
      );
    }

    return ValueListenableBuilder(
        valueListenable: layersBox.listenable(),
        builder: (context, Box box, _) {
          return ReorderableListView.builder(
            proxyDecorator: proxyDecorator,
            padding: nested
                ? const EdgeInsets.all(0)
                : const EdgeInsets.fromLTRB(0, 0, 0, 200),
            shrinkWrap: true,
            physics: nested ? const ClampingScrollPhysics() : null,
            buildDefaultDragHandles: true,
            itemCount: layers.length,
            itemBuilder: (context, listIndex) {
              return layers[listIndex].getWidget(Key("$listIndex"));
            },
            onReorder: (int oldIndex, int newIndex) {
              if (newIndex > oldIndex) {
                newIndex -= 1;
              }
              var oldItem = layers.removeAt(oldIndex);
              layers.insert(newIndex, oldItem);
            },
          );
        });
  }
}
