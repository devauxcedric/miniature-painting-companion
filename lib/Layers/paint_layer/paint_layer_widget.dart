import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/extensions/string_extension.dart';
import 'package:miniature_painting_companion/utils.dart';

import 'new_paint_layer_dialog.dart';

class PaintLayerWidget extends StatelessWidget {
  final PaintLayer layer;

  const PaintLayerWidget({super.key, required this.layer});

  delete() {
    layer.delete();
  }

  Future<void> _showNoteDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.note),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(layer.note),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _getPaintImage() {
    if (layer.paint.image == null) {
      return Utils.getSvgIconSized(50, 50, "placeholderFilled");
    } else {
      return SvgPicture.string(layer.paint.image!);
    }
  }

  @override
  Widget build(BuildContext context) {
    var cardContent = Row(
      children: [
        const SizedBox(width: 8),
        Visibility(
            visible: layer.note.isNotEmpty,
            child: IconButton(
              icon: Utils.getSvgIconSized(35, 35, "note"),
              onPressed: () {
                _showNoteDialog(context);
              },
            )),
        Expanded(
            child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              const SizedBox(height: 8),
              Utils.gothicText(layer.name),
              const SizedBox(height: 8),
              Text(layer.applicationType.name.camelToSentence().capitalize()),
              Text("${layer.dilution}%"),
              const SizedBox(height: 8),
            ],
          ),
        )),
        Row(
          children: [
            const SizedBox(width: 18),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 8),
                _getPaintImage(),
                const SizedBox(height: 8),
                Visibility(
                    visible: layer.paint.type.isNotEmpty,
                    child: Row(children: [
                      Utils.gothicText(layer.paint.type.toUpperCase()),
                      const SizedBox(height: 8),
                    ])),
                const SizedBox(height: 8),
              ],
            ),
            const SizedBox(width: 18),
          ],
        ),
      ],
    );

    //var card = Card(child: cardContent);

    return Card(
      child: Slidable(
          startActionPane: ActionPane(
            motion: const BehindMotion(),
            children: [
              SlidableAction(
                onPressed: (context) => {layer.delete()},
                backgroundColor: Colors.red,
                foregroundColor: Colors.white,
                icon: Icons.delete,
              ),
            ],
          ),
          endActionPane: ActionPane(
            motion: const BehindMotion(),
            children: [
              SlidableAction(
                onPressed: (otherContext) => NewPaintLayerDialog.show(
                    context, null, layer, () => layer.save()),
                backgroundColor: Colors.green,
                foregroundColor: Colors.white,
                icon: Icons.edit,
              ),
            ],
          ),
          child: cardContent),
    );
  }
}
