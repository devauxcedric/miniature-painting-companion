import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/design/textual_button.dart';

import '../../design/create_or_update_dialog.dart';
import '../../design/named_slider.dart';
import '../../design/paint_drop_down.dart';
import '../../main.dart';

class PaintLayerCreationWidget extends StatefulWidget {
  final PaintLayer? layerToEdit;
  final TextEditingController nameController;

  const PaintLayerCreationWidget(
      {super.key, this.layerToEdit, required this.nameController});

  @override
  State<StatefulWidget> createState() {
    return PaintLayerCreationState();
  }
}

class PaintLayerCreationState extends State<PaintLayerCreationWidget> {
  late final TextEditingController paintTypeController =
      TextEditingController(text: widget.layerToEdit?.paint.type);
  late final TextEditingController paintNameController = widget.nameController;
  late final TextEditingController paintImageController =
      TextEditingController(text: widget.layerToEdit?.paint.image);
  late final TextEditingController noteController =
      TextEditingController(text: widget.layerToEdit?.note);

  late final ValueNotifier<ApplicationType> paintNotifier =
      ValueNotifier((ApplicationType.normal));

  late final ValueNotifier<int> dilutionNotifier =
      ValueNotifier(widget.layerToEdit?.dilution ?? 100);

  var showColorPicker = true;

  @override
  Widget build(BuildContext context) {
    var l = AppLocalizations.of(context)!;

    var paints = groupBy(paintsBox.values, (paint) => paint.type);
    var types = paints.keys;

    return showColorPicker
        ? DefaultTabController(
            length: types.length,
            child: SizedBox(
              height: 400,
              width: 350,
              child: Scaffold(
                appBar: TabBar(
                  isScrollable: true,
                  tabs: types
                      .map((type) => Tab(
                            text: type,
                          ))
                      .toList(),
                ),
                body: Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TabBarView(
                    //physics: NeverScrollableScrollPhysics(),
                    children: types.map((typeAct) {
                      var typePaints = paints[typeAct]!.sorted(
                          (act, other) => act.name.compareTo(other.name));
                      return GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  mainAxisExtent: 128,
                                  crossAxisSpacing: 8,
                                  mainAxisSpacing: 8,
                                  crossAxisCount: 4),
                          itemCount: typePaints.length,
                          itemBuilder: (context, index) {
                            var actual = typePaints[index];
                            return Material(
                              clipBehavior: Clip.antiAlias,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    showColorPicker = false;
                                    paintNameController.text = actual.name;
                                    paintImageController.text =
                                        actual.image == null
                                            ? ""
                                            : actual.image!;
                                    paintTypeController.text = actual.type;
                                  });
                                },
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SvgPicture.string(actual.image!),
                                    Text.rich(
                                        textAlign: TextAlign.center,
                                        overflow: TextOverflow.clip,
                                        TextSpan(text: actual.name))
                                  ],
                                ),
                              ),
                            );
                          });
                    }).toList(),
                  ),
                ),
              ),
            ),
          )
        : Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Autocomplete<Paint>(
                      fieldViewBuilder: (context, textEditingController,
                              focusNode, onFieldSubmitted) =>
                          TextFormField(
                        onChanged: (text) {
                          paintNameController.text = text;
                          textEditingController.text = text;
                        },
                        controller: paintNameController,
                        onFieldSubmitted: (String value) {
                          onFieldSubmitted();
                        },
                        focusNode: focusNode,
                        decoration: InputDecoration(
                          labelText: l.paintName,
                        ),
                      ),
                      displayStringForOption: (Paint paint) => paint.name,
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        if (textEditingValue.text == '') {
                          return const Iterable<Paint>.empty();
                        }
                        return paintsBox.values.where((Paint option) {
                          return option.name
                              .toLowerCase()
                              .contains(textEditingValue.text.toLowerCase());
                        });
                      },
                      onSelected: (Paint selection) {
                        paintNameController.text = selection.name;
                        paintImageController.text =
                            selection.image == null ? "" : selection.image!;
                        paintTypeController.text = selection.type;
                      },
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  TextualButton(
                      onPressed: () {
                        setState(() {
                          showColorPicker = true;
                        });
                      },
                      iconName: "color_picker")
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  children: [
                    Text.rich(
                      TextSpan(text: "Application"),
                      textAlign: TextAlign.start,
                    ),
                    Expanded(
                        child: Align(
                      alignment: Alignment.centerRight,
                      child: PaintDropdown(paintNotifier),
                    ))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: TextFormField(
                  controller: paintTypeController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                    hintText: l.paintType,
                    labelText: l.paintType,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  children: [
                    Text.rich(
                      TextSpan(text: l.dilution),
                      textAlign: TextAlign.start,
                    ),
                    NamedSlider(
                      notifier: dilutionNotifier,
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: TextFormField(
                  controller: noteController,
                  decoration: InputDecoration(
                    labelText: l.note,
                    hintText: l.note,
                  ),
                ),
              ),
            ],
          );
  }
}

class NewPaintLayerDialog {
  static show(dialogContext, HiveList? paintLayers, PaintLayer? layerToEdit,
      Function() saveFunction) {
    var l = AppLocalizations.of(dialogContext)!;

    var key = GlobalKey<PaintLayerCreationState>();
    TextEditingController nameController =
        TextEditingController(text: layerToEdit?.paint.name);

    var content = [
      PaintLayerCreationWidget(
        key: key,
        layerToEdit: layerToEdit,
        nameController: nameController,
      )
    ];

    void onUpdate(BuildContext context, TextEditingValue value,
        TextEditingController controller) {
      var creationState = key.currentState!;
      var paint = paintsBox.get(creationState.paintNameController.text,
          defaultValue: Paint(creationState.paintTypeController.text,
              creationState.paintNameController.text, null))!;

      layerToEdit?.name = creationState.paintNameController.text;
      layerToEdit?.paint = paint;
      layerToEdit?.note = creationState.noteController.text;
      layerToEdit?.applicationType = creationState.paintNotifier.value;
      layerToEdit?.dilution = creationState.dilutionNotifier.value.toInt();
      layerToEdit?.save();
    }

    void onSave(BuildContext context, TextEditingValue value,
        TextEditingController controller) {
      var creationState = key.currentState!;
      var paint = paintsBox.get(creationState.paintNameController.text,
          defaultValue: Paint(creationState.paintTypeController.text,
              creationState.paintNameController.text, null))!;
      var createdLayer = PaintLayer(
          creationState.paintNameController.text,
          paint,
          creationState.noteController.text,
          creationState.paintNotifier.value,
          creationState.dilutionNotifier.value.toInt());
      layersBox.add(createdLayer);
      paintLayers!.add(createdLayer);
      saveFunction();
    }

    CreateOrUpdateDialog.show(dialogContext, layerToEdit, nameController,
        content, l.createLayer, l.updateLayer, onSave, onUpdate);
  }
}
