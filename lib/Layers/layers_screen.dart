import 'package:flutter/material.dart';
import 'package:miniature_painting_companion/Layers/combined/new_combined_layer_dialog.dart';
import 'package:miniature_painting_companion/Layers/sticker/new_sticker_layer_dialog.dart';
import 'package:miniature_painting_companion/Models/model_interfaces.dart';
import 'package:miniature_painting_companion/utils.dart';

import '../Models/hive_models.dart';
import 'layers_list_widget.dart';
import 'paint_layer/new_paint_layer_dialog.dart';
import 'separator/new_separator_layer_dialog.dart';

class LayersScreen extends StatelessWidget {
  final PaintJob paintJob;

  const LayersScreen({super.key, required this.paintJob});

  Widget getBody() {
    paintJob.paintLayers;
    return LayersList(
        layers: paintJob.paintLayers.cast<IDrawableLayer>(), nested: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(paintJob.name),
        ),
        body: getBody(),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              height: 40.0,
              width: 40.0,
              child: FloatingActionButton(
                onPressed: () => {
                  NewStickerLayerDialog.show(context, paintJob, null),
                  paintJob.save()
                },
                // TODO internationalisation
                tooltip: 'Add sticker',
                heroTag: "sticker",
                child: Utils.getSvgIconSized(20, 20, "plus"),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 40.0,
              width: 40.0,
              child: FloatingActionButton(
                onPressed: () => {
                  NewLayerSeparatorDialog.show(context, paintJob, null),
                  paintJob.save()
                },
                tooltip: 'Add separator',
                heroTag: "separator",
                child: Utils.getSvgIconSized(20, 20, "separator"),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 40.0,
              width: 40.0,
              child: FloatingActionButton(
                onPressed: () => {
                  NewCombinedLayerDialog.show(context, paintJob, null),
                  paintJob.save()
                },
                tooltip: 'Add combined paint',
                heroTag: "combined",
                child: Utils.getSvgIconSized(30, 30, "plusCombined"),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            FloatingActionButton(
              onPressed: () => {
                NewPaintLayerDialog.show(
                    context, paintJob.paintLayers, null, () => paintJob.save())
              },
              tooltip: 'Add paint job',
              heroTag: "paintJob",
              child: Utils.getSvgIconSized(40, 40, "plus"),
            ),
          ],
        ));
  }
}
