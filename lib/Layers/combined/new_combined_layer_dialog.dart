import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive/hive.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/main.dart';

import '../../design/create_or_update_dialog.dart';

class NewCombinedLayerDialog {
  static show(context, PaintJob? paintLayer, CombinedLayer? combinedToEdit) {
    var l = AppLocalizations.of(context)!;

    TextEditingController nameController =
        TextEditingController(text: combinedToEdit?.name);

    var content = [
      TextFormField(
        autofocus: true,
        controller: nameController,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: l.name,
          labelText: l.name,
        ),
      ),
    ];

    onUpdate(BuildContext context, TextEditingValue value,
            TextEditingController controller) =>
        {combinedToEdit?.name = nameController.text, combinedToEdit?.save()};

    void onSave(BuildContext context, TextEditingValue value,
        TextEditingController controller) {
      var createdLayer =
          CombinedLayer(name: controller.text, layers: HiveList(layersBox));
      layersBox.add(createdLayer);
      paintLayer?.paintLayers.add(createdLayer);
      paintLayer?.save();
    }

    CreateOrUpdateDialog.show(context, combinedToEdit, nameController, content,
        l.createCombinedPaint, l.updateCombinedPaint, onSave, onUpdate);
  }
}
