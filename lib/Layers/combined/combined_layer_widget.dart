import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:miniature_painting_companion/Layers/paint_layer/new_paint_layer_dialog.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/Models/model_interfaces.dart';
import 'package:miniature_painting_companion/utils.dart';

import '../layers_list_widget.dart';
import 'new_combined_layer_dialog.dart';

class CombinedLayerWidget extends StatelessWidget {
  final CombinedLayer layer;

  const CombinedLayerWidget({super.key, required this.layer});

  @override
  Widget build(BuildContext context) {
    var addIcon = Utils.getSvgIconSized(30, 30, "plus");

    var body = Column(
      children: [
        const SizedBox(height: 8),
        Center(
          child: Utils.gothicText(layer.name.toUpperCase()),
        ),
        const SizedBox(height: 8),
        LayersList(layers: layer.layers!.cast<IDrawableLayer>(), nested: true),
        Ink(
          decoration: const ShapeDecoration(
            color: Colors.blueGrey,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: addIcon,
            onPressed: () => {
              layer.layers,
              NewPaintLayerDialog.show(
                  context, layer.layers, null, () => layer.save()),
              layer.save()
            },
          ),
        ),
        const SizedBox(height: 8),
      ],
    );

    return Slidable(
        startActionPane: ActionPane(
          motion: const BehindMotion(),
          children: [
            SlidableAction(
              onPressed: (context) => {layer.delete()},
              backgroundColor: Colors.red,
              foregroundColor: Colors.white,
              icon: Icons.delete,
            ),
          ],
        ),
        endActionPane: ActionPane(
          motion: const BehindMotion(),
          children: [
            SlidableAction(
              onPressed: (otherContext) =>
                  NewCombinedLayerDialog.show(context, null, layer),
              backgroundColor: Colors.green,
              foregroundColor: Colors.white,
              icon: Icons.edit,
            ),
          ],
        ),
        child: Card(
          child: body,
        ));
  }
}
