import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_picker/image_picker.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/main.dart';

import '../../design/create_or_update_dialog.dart';
import '../../design/image_picking_widget.dart';
import '../../design/updatable_image.dart';
import '../../utils.dart';

class NewStickerLayerDialog {
  static show(context, PaintJob? paintJob, StickerLayer? stickerToEdit) {
    var l = AppLocalizations.of(context)!;

    TextEditingController nameController =
        TextEditingController(text: stickerToEdit?.name);

    final GlobalKey<UpdatableImageState> imageKey = GlobalKey();

    var content = [
      ImagePickingWidget(
          imageKey: imageKey, initialValue: stickerToEdit?.imagePath),
      TextFormField(
        autofocus: true,
        controller: nameController,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: l.name,
          labelText: l.name,
        ),
      ),
    ];

    Future<void> onUpdate(BuildContext context, TextEditingValue value,
        TextEditingController controller) async {
      stickerToEdit?.name = nameController.text;
      if (imageKey.currentState?.imagePath != null) {
        stickerToEdit?.imagePath = (await Utils.moveFileToAppStorage(
                XFile(imageKey.currentState!.imagePath!)))
            ?.path;
      }
      stickerToEdit?.save();
    }

    Future<void> onSave(BuildContext context, TextEditingValue value,
        TextEditingController controller) async {
      var createdLayer = StickerLayer(
          nameController.text,
          imageKey.currentState?.imagePath == null
              ? null
              : (await Utils.moveFileToAppStorage(
                      XFile(imageKey.currentState!.imagePath!)))
                  ?.path);
      layersBox.add(createdLayer);
      paintJob?.paintLayers.add(createdLayer);
      paintJob?.save();
    }

    CreateOrUpdateDialog.show(context, stickerToEdit, nameController, content,
        l.createSticker, l.updateSticker, onSave, onUpdate);
  }
}
