import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:miniature_painting_companion/Layers/sticker/new_sticker_layer_dialog.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/utils.dart';

class StickerLayerWidget extends StatelessWidget {
  final StickerLayer sticker;

  const StickerLayerWidget({super.key, required this.sticker});

  @override
  Widget build(BuildContext context) {
    Utils.getSvgIconSized(20, 30, "aquila");

    var content = Center(
        child: Row(
      children: [
        const SizedBox(width: 8),
        Expanded(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
            child: Text(
              sticker.name.toUpperCase(),
              textAlign: TextAlign.center,
              style: const TextStyle(fontFamily: 'Gothic'),
            ),
          ),
        ),
        Card(
          shadowColor: Colors.transparent,
          clipBehavior: Clip.hardEdge,
          child: Utils.getImageOrPlaceholderSized(80, 80, sticker.imagePath),
        ),
        const SizedBox(width: 40),
      ],
    ));

    return Card(
        child: Slidable(
      startActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            onPressed: (context) => {sticker.delete()},
            backgroundColor: const Color(0xFFFE4A49),
            foregroundColor: Colors.white,
            icon: Icons.delete,
          ),
        ],
      ),
      endActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            onPressed: (otherContext) =>
                NewStickerLayerDialog.show(context, null, sticker),
            backgroundColor: Colors.green,
            foregroundColor: Colors.white,
            icon: Icons.edit,
          ),
        ],
      ),
      child: content,
    ));
  }
}
