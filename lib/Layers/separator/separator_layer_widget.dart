import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:miniature_painting_companion/Layers/separator/new_separator_layer_dialog.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/utils.dart';

class SeparatorLayerWidget extends StatelessWidget {
  final LayerSeparator separator;

  const SeparatorLayerWidget({super.key, required this.separator});

  @override
  Widget build(BuildContext context) {
    var aquila = Utils.getSvgIconSized(20, 30, "aquila");

    var content = Center(
        child: Row(
      children: [
        const SizedBox(width: 8),
        aquila,
        Expanded(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
            child: Text(
              separator.text.toUpperCase(),
              textAlign: TextAlign.center,
              style: const TextStyle(fontFamily: 'Gothic'),
            ),
          ),
        ),
        aquila,
        const SizedBox(width: 8),
      ],
    ));

    return Card(
        child: Slidable(
      startActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            onPressed: (context) => {separator.delete()},
            backgroundColor: const Color(0xFFFE4A49),
            foregroundColor: Colors.white,
            icon: Icons.delete,
          ),
        ],
      ),
      endActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            onPressed: (otherContext) =>
                NewLayerSeparatorDialog.show(context, null, separator),
            backgroundColor: Colors.green,
            foregroundColor: Colors.white,
            icon: Icons.edit,
          ),
        ],
      ),
      child: content,
    ));
  }
}
