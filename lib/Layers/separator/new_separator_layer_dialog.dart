import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive/hive.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/Models/layers_model.dart';
import 'package:miniature_painting_companion/main.dart';

import '../../design/create_or_update_dialog.dart';

class NewLayerSeparatorDialog {
  static show(context, PaintJob? paintLayer, LayerSeparator? separatorToEdit) {
    var l = AppLocalizations.of(context)!;

    TextEditingController paintTypeController =
        TextEditingController(text: separatorToEdit?.text);

    var content = [
      TextFormField(
        autofocus: true,
        controller: paintTypeController,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: l.name,
          labelText: l.name,
        ),
      ),
    ];

    onUpdate(BuildContext context, TextEditingValue value,
            TextEditingController controller) =>
        {
          separatorToEdit?.text = paintTypeController.text,
          separatorToEdit?.save()
        };

    void onSave(BuildContext context, TextEditingValue value,
        TextEditingController controller) {
      paintLayer?.paintLayers ??= HiveList(layersBox);

      var createdLayer = LayerSeparator(paintTypeController.text);
      layersBox.add(createdLayer);
      paintLayer?.paintLayers?.add(createdLayer);
      paintLayer?.save();
    }

    CreateOrUpdateDialog.show(context, separatorToEdit, paintTypeController,
        content, l.createSeparator, l.updateSeparator, onSave, onUpdate);
  }
}
