import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/export_data.dart';
import 'package:miniature_painting_companion/miniature/new_miniature_dialog.dart';
import 'package:miniature_painting_companion/utils.dart';

import 'Models/layers_model.dart';
import 'Models/model_interfaces.dart';
import 'miniature/miniature_widget.dart';

late Box<Miniature> miniaturesBox;
late Box<HistoryImage> historyImagesBox;
late Box<PaintJob> paintJobBox;
late Box<IDrawableLayer> layersBox;
late Box<Paint> paintsBox;

Future<void> main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(MiniatureAdapter());
  Hive.registerAdapter(PaintJobAdapter());
  Hive.registerAdapter(PaintLayerAdapter());
  Hive.registerAdapter(ApplicationTypeAdapter());
  Hive.registerAdapter(HistoryImageAdapter());
  Hive.registerAdapter(PaintAdapter());
  Hive.registerAdapter(LayerSeparatorAdapter());
  Hive.registerAdapter(CombinedLayerAdapter());
  Hive.registerAdapter(StickerLayerAdapter());

  paintsBox = await Hive.openBox<Paint>("paints");
  layersBox = await Hive.openBox<IDrawableLayer>("layers");
  paintJobBox = await Hive.openBox<PaintJob>("paintJobs");
  historyImagesBox = await Hive.openBox<HistoryImage>("historyImages");
  miniaturesBox = await Hive.openBox<Miniature>("miniatures");

  loadPaintCsv();

  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}

void loadPaintCsv() async {
  final rawData = await rootBundle.loadString("assets/paints_images.csv");

  List<List<dynamic>> listData =
      const CsvToListConverter(eol: '\n').convert(rawData);
  for (var rowAct in listData) {
    if (!paintsBox.containsKey(rowAct[1])) {
      paintsBox.put(
          rowAct[1].toString(),
          Paint(rowAct[0].toString(), rowAct[1].toString(),
              rowAct[2].toString()));
    }
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Minis companion',
      theme: ThemeData(
          cardTheme: CardTheme(clipBehavior: Clip.antiAlias),
          inputDecorationTheme:
              InputDecorationTheme(border: OutlineInputBorder()),
          colorSchemeSeed: Colors.grey),
      darkTheme: ThemeData(
          cardTheme: CardTheme(clipBehavior: Clip.antiAlias),
          colorSchemeSeed: Colors.grey,
          brightness: Brightness.dark),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: const MyHomePage(title: 'Minis companion'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
  }

  bool sortByName = false;

  List<Miniature> getSortedContent() {
    var toSort = miniaturesBox.values.toList();
    if (sortByName) {
      toSort.sort((a, b) => a.name.compareTo(b.name));
    }
    return toSort;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            onPressed: () => setState(() {
              sortByName = !sortByName;
            }),
            icon: sortByName
                ? const Icon(Icons.sort_by_alpha)
                : const Icon(Icons.sort),
          )
        ],
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.blueGrey,
              ),
              child:
                  Utils.gothicText(AppLocalizations.of(context)!.drawerHeader),
            ),
            Row(
              children: [
                Expanded(
                  child: ListTile(
                    title: Text(AppLocalizations.of(context)!.exportData),
                    onTap: () => {DataExporter.exportData(context)},
                  ),
                ),
                Utils.getSvgIconSized(20, 30, "aquila"),
                const SizedBox(width: 20)
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: ListTile(
                    title: Text(AppLocalizations.of(context)!.importData),
                    onTap: () => {DataExporter.importData(context)},
                  ),
                ),
                Utils.getSvgIconSized(20, 30, "aquila"),
                const SizedBox(width: 20)
              ],
            ),
          ],
        ),
      ),
      body: ValueListenableBuilder(
          valueListenable: miniaturesBox.listenable(),
          builder: (context, Box box, _) {
            return GridView.builder(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 100),
              itemCount: box.length,
              itemBuilder: (context, listIndex) {
                return MiniatureWidget(
                    miniatures: getSortedContent(), index: listIndex);
              },
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () => NewMiniatureDialog.show(context, null),
        tooltip: AppLocalizations.of(context)!.addMiniature,
        child: Utils.getSvgIconSized(40, 40, "plus"),
      ),
    );
  }
}
