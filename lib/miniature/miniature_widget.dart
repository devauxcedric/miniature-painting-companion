import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/PaintJob/paint_jobs_list_widget.dart';
import 'package:miniature_painting_companion/design/custom_pop_up_menu.dart';
import 'package:miniature_painting_companion/design/image_views.dart';

import '../design/tittle_card.dart';
import 'new_miniature_dialog.dart';

class MiniatureWidget extends StatelessWidget {
  final List<Miniature> miniatures;
  final int index;
  late final Miniature _miniature;

  MiniatureWidget({super.key, required this.miniatures, required this.index}) {
    _miniature = miniatures[index];
  }

  @override
  Widget build(BuildContext context) {
    return TittleCard(
      imagePath: _miniature.imagePath,
      title: _miniature.name,
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PaintJobList(
                    miniature: _miniature,
                  )),
        );
      },
      popUp: CustomPopUpMenu(
          displayOptionals: _miniature.imagePath != null,
          items: [
            (
              key: "?view",
              value: AppLocalizations.of(context)!.view,
              action: () {
                var miniWithImages =
                    miniatures.where((mini) => mini.imagePath != null);
                var current = miniWithImages.indexed
                    .firstWhere((mini) => mini.$2.key == _miniature.key);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ImageViews(
                        miniWithImages
                            .map((mini) => mini.imagePath)
                            .nonNulls
                            .toList(),
                        initialPage: current.$1,
                        name: _miniature.name),
                  ),
                );
              }
            ),
            (
              key: "edit",
              value: AppLocalizations.of(context)!.edit,
              action: () {
                NewMiniatureDialog.show(context, _miniature);
              }
            ),
            (
              key: "clone",
              value: AppLocalizations.of(context)!.clone,
              action: () {
                _miniature.clone();
              }
            ),
            (
              key: "delete",
              value: AppLocalizations.of(context)!.delete,
              action: () {
                _miniature.delete();
              }
            )
          ]),
    );
  }
}
