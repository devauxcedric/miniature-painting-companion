import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:miniature_painting_companion/design/updatable_image.dart';
import 'package:miniature_painting_companion/main.dart';

import '../Models/hive_models.dart';
import '../design/create_or_update_dialog.dart';
import '../design/image_picking_widget.dart';
import '../utils.dart';

class NewMiniatureDialog {
  static show(context, Miniature? toUpdate) {
    var l = AppLocalizations.of(context)!;
    TextEditingController nameController =
        TextEditingController(text: toUpdate?.name);

    final GlobalKey<UpdatableImageState> imageKey = GlobalKey();

    var content = [
      ImagePickingWidget(imageKey: imageKey, initialValue: toUpdate?.imagePath),
      TextFormField(
        controller: nameController,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: l.name,
          labelText: l.name,
        ),
      ),
    ];

    onUpdate(BuildContext context, TextEditingValue value,
            TextEditingController controller) async =>
        {
          toUpdate?.name = controller.text,
          toUpdate?.imagePath = imageKey.currentState?.imagePath,
          if (imageKey.currentState?.imagePath != null)
            {
              toUpdate?.imagePath = (await Utils.moveFileToAppStorage(
                      XFile(imageKey.currentState!.imagePath!)))
                  ?.path,
            },
          toUpdate?.save()
        };

    onSave(BuildContext context, TextEditingValue value,
            TextEditingController controller) async =>
        {
          miniaturesBox.add(Miniature(
              controller.text,
              "",
              imageKey.currentState?.imagePath == null
                  ? null
                  : (await Utils.moveFileToAppStorage(
                          XFile(imageKey.currentState!.imagePath!)))
                      ?.path,
              HiveList(paintJobBox),
              HiveList(historyImagesBox)))
        };

    CreateOrUpdateDialog.show(context, toUpdate, nameController, content,
        l.createMiniature, l.updateMiniature, onSave, onUpdate);
  }
}
