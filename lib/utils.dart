import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pro_image_editor/models/editor_callbacks/pro_image_editor_callbacks.dart';
import 'package:pro_image_editor/models/editor_configs/pro_image_editor_configs.dart';
import 'package:pro_image_editor/modules/main_editor/main_editor.dart';

typedef PopUpItem = ({String key, String value});

class Utils {
  static Widget getImageOrPlaceholder(String? imagePath) {
    if (imagePath != null) {
      return Ink.image(
        image: Image.file(File(imagePath)).image,
        fit: BoxFit.cover,
      );
    }

    return getSvgIcon("placeholder");
  }

  static Widget getImageOrPlaceholderSized(
    double height,
    double width,
    String? imagePath,
  ) {
    if (imagePath != null) {
      return SizedBox(
          height: height,
          width: width,
          child: Ink.image(
            image: Image.file(
                    height: height,
                    width: width,
                    cacheHeight: height.toInt(),
                    cacheWidth: width.toInt(),
                    File(imagePath))
                .image,
            fit: BoxFit.cover,
          ));
    }

    return Utils.getSvgIconSized(height, width, "placeholder");
  }

  static SizedBox getSvgIconSized(double height, double width, String name) {
    return SizedBox(
      height: height,
      width: width,
      child: SvgPicture.asset("assets/$name.svg", semanticsLabel: 'name'),
    );
  }

  static SvgPicture getSvgIcon(String name) {
    return SvgPicture.asset("assets/$name.svg", semanticsLabel: 'name');
  }

  static SizedBox getPngIcon(double height, double width, String name) {
    return SizedBox(
      height: height,
      width: width,
      child: Image.asset("assets/$name.png"),
    );
  }

  static Text gothicText(String text) {
    return Text(
      text,
      style: const TextStyle(fontFamily: 'Gothic'),
    );
  }

  static List<PopupMenuEntry<String>> getPopUpItems(
      List<PopUpItem> items, bool displayOptionals) {
    List<PopupMenuEntry<String>> entries = [];
    for (PopUpItem toCreate in items) {
      if (!toCreate.key.startsWith("?") || displayOptionals) {
        entries.add(PopupMenuItem<String>(
          value: toCreate.key,
          child: Text(toCreate.value),
        ));
      }
    }

    return entries;
  }

  static Future<XFile?> pickImage(ImageSource source) async {
    if (source != ImageSource.camera ||
        await Permission.camera.request().isGranted) {
      final ImagePicker picker = ImagePicker();
      return await picker.pickImage(source: source, requestFullMetadata: true);
    }

    Fluttertoast.showToast(
        msg: "Camera permission denied, can't take a picture",
        toastLength: Toast.LENGTH_LONG);
    return null;
  }

  static Future<List<XFile>?> pickImages(ImageSource source) async {
    if (source != ImageSource.camera ||
        await Permission.camera.request().isGranted) {
      final ImagePicker picker = ImagePicker();
      return await picker.pickMultiImage(requestFullMetadata: true);
    }

    Fluttertoast.showToast(
        msg: "Camera permission denied, can't take a picture",
        toastLength: Toast.LENGTH_LONG);
    return null;
  }

  static void showImageEditor(String imagePath, BuildContext context,
      {VoidCallback? additionalCallback}) {
    var imageFile = File(imagePath);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProImageEditor.file(
          imageFile,
          configs: ProImageEditorConfigs(
              filterEditorConfigs: FilterEditorConfigs(enabled: false),
              blurEditorConfigs: BlurEditorConfigs(enabled: false),
              emojiEditorConfigs: EmojiEditorConfigs(enabled: false)),
          callbacks: ProImageEditorCallbacks(
              onImageEditingComplete: (Uint8List bytes) async {
            imageFile.writeAsBytesSync(bytes);
            await FileImage(imageFile).evict();
            if (additionalCallback != null) {
              additionalCallback();
            }
            Navigator.pop(context);
          }),
        ),
      ),
    );
  }

  static Future<XFile?> moveFileToAppStorage(XFile? imageFile) async {
    if (imageFile != null) {
      var appDir = (await getApplicationDocumentsDirectory()).path;
      File(imageFile.path).renameSync("$appDir/${imageFile.name}");
      return XFile("$appDir/${imageFile.name}");
    }
    return null;
  }
}
