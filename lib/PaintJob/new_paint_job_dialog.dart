import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/design/image_picking_widget.dart';
import 'package:miniature_painting_companion/main.dart';

import '../design/create_or_update_dialog.dart';
import '../design/updatable_image.dart';
import '../utils.dart';

class NewPaintJobDialog {
  static show(context, Miniature? miniature, PaintJob? toUpdate) {
    var l = AppLocalizations.of(context)!;
    TextEditingController paintJobController =
        TextEditingController(text: toUpdate?.name);

    final GlobalKey<UpdatableImageState> imageKey = GlobalKey();

    var content = [
      ImagePickingWidget(imageKey: imageKey, initialValue: toUpdate?.imagePath),
      TextFormField(
        controller: paintJobController,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: l.name,
          labelText: l.name,
        ),
      ),
    ];

    onUpdate(BuildContext context, TextEditingValue value,
            TextEditingController controller) async =>
        {
          toUpdate?.name = controller.text,
          if (imageKey.currentState?.imagePath != null)
            {
              toUpdate?.imagePath = (await Utils.moveFileToAppStorage(
                      XFile(imageKey.currentState!.imagePath!)))
                  ?.path,
            },
          toUpdate?.save()
        };

    onSave(BuildContext context, TextEditingValue value,
            TextEditingController controller) async =>
        {
          (Miniature? miniature, String? imagePath,
              TextEditingController paintJobController) async {
            var paintJob = PaintJob(
                imagePath: imageKey.currentState?.imagePath == null
                    ? null
                    : (await Utils.moveFileToAppStorage(
                            XFile(imageKey.currentState!.imagePath!)))
                        ?.path,
                name: paintJobController.text,
                paintLayers: HiveList(layersBox));
            paintJobBox.add(paintJob);
            miniature?.paintJobs.add(paintJob);
            miniature?.save();
          }(miniature, imageKey.currentState?.imagePath, paintJobController)
        };

    CreateOrUpdateDialog.show(context, toUpdate, paintJobController, content,
        l.createPaintJob, l.updatePaintJob, onSave, onUpdate);
  }
}
