import 'dart:io';

import 'package:exif/exif.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:miniature_painting_companion/design/image_views.dart';
import 'package:miniature_painting_companion/design/updatable_image.dart';

import '../Models/hive_models.dart';
import '../main.dart';
import '../utils.dart';

final logger = Logger();

class MiniatureHistoryList extends StatefulWidget {
  final Miniature miniature;

  const MiniatureHistoryList({super.key, required this.miniature});

  @override
  State<MiniatureHistoryList> createState() => _MiniatureHistoryListState();
}

class _MiniatureHistoryListState extends State<MiniatureHistoryList> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: historyImagesBox.listenable(),
        builder: (context, Box box, _) {
          List<Widget> content = [];

          List<HistoryImage>? images =
              widget.miniature.historyImages.cast<HistoryImage>();
          // Will put the most recent first
          images.sort((a, b) => b.importTime.compareTo(a.importTime));

          images.asMap().forEach((index, element) =>
              content.add(MiniatureHistoryCard(index: index, image: images)));
          content.add(AddMiniatureHistoryCard(
            index: 0,
            miniature: widget.miniature,
          ));

          return Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: ConstrainedBox(
                constraints: const BoxConstraints(maxHeight: 150),
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemExtent: 150,
                    itemCount: content.length,
                    itemBuilder: (BuildContext context, int index) {
                      return content[index];
                    })),
          );
        });
  }
}

class MiniatureHistoryCard extends StatelessWidget {
  const MiniatureHistoryCard({
    super.key,
    required this.index,
    required this.image,
  });

  final int index;
  final List<HistoryImage> image;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<UpdatableImageState> imageKey = GlobalKey();
    var displayed =
        UpdatableImage(key: imageKey, initialValue: image[index].imagePath);

    return PopupMenuButton<String>(
      position: PopupMenuPosition.under,
      onSelected: (String result) async {
        if (result == "view") {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ImageViews(
                image.map((image) => image.imagePath).toList(),
                name: image[index].importTime.toString(),
                initialPage: index,
              ),
            ),
          );
        }
        if (result == "edit") {
          Utils.showImageEditor(image[index].imagePath, context,
              additionalCallback: () => {
                    image[index].save(),
                    imageKey.currentState?.changeImage(image[index].imagePath)
                  });
        }
        if (result == "date") {
          final DateTime? picked = await showDatePicker(
              context: context,
              initialDate: image[index].importTime,
              firstDate: DateTime(2015, 8),
              lastDate: DateTime(2101));
          if (picked != null) {
            image[index].importTime = picked;
            image[index].save();
          }
        } else if (result == "delete") {
          image[index].delete();
        }
      },
      itemBuilder: (BuildContext context) => Utils.getPopUpItems([
        (key: "view", value: AppLocalizations.of(context)!.view),
        (key: "edit", value: AppLocalizations.of(context)!.edit),
        (key: "date", value: AppLocalizations.of(context)!.date),
        (key: "delete", value: AppLocalizations.of(context)!.delete)
      ], true),
      child: Card(child: displayed),
    );
  }
}

class AddMiniatureHistoryCard extends StatelessWidget {
  const AddMiniatureHistoryCard({
    super.key,
    required this.index,
    required this.miniature,
  });

  final int index;
  final Miniature miniature;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
          onLongPress: () async {
            (await Utils.pickImages(ImageSource.gallery))
                ?.forEach((path) => createHistoryImage(path, context));
          },
          onTap: () async {
            var path = await Utils.pickImage(ImageSource.camera);
            createHistoryImage(path, context);
          },
          child: Container(
            padding: const EdgeInsets.all(40),
            child: Utils.getSvgIconSized(40, 40, "plus"),
          )),
    );
  }

  Future<DateTime?> getImageTimeTaken(XFile file, BuildContext context) async {
    var exif = await readExifFromFile(File(file.path));

    try {
      var timeTaken = exif.entries
          .firstWhere((elem) => elem.key == 'Image DateTime')
          .value
          .printable;
      DateFormat format = DateFormat("yyyy:MM:dd hh:mm:ss");

      return format.parse(timeTaken);
    } catch (e) {
      logger.w("Could not parser dateTime", error: e);
      Fluttertoast.showToast(
          // TODO will need to check this
          msg: AppLocalizations.of(context)!.errorWhileParsingDate,
          toastLength: Toast.LENGTH_LONG);
      return null;
    }
  }

  Future<void> createHistoryImage(
      XFile? imageFile, BuildContext context) async {
    if (imageFile != null) {
      var date = await getImageTimeTaken(imageFile, context) ?? DateTime.now();

      var historyImage = HistoryImage(
          (await Utils.moveFileToAppStorage(imageFile))!.path, date);
      historyImagesBox.add(historyImage);
      miniature.historyImages.add(historyImage);
      miniature.save();
    }
  }
}
