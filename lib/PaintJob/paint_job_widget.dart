import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:miniature_painting_companion/Layers/layers_screen.dart';
import 'package:miniature_painting_companion/Models/hive_models.dart';
import 'package:miniature_painting_companion/design/custom_pop_up_menu.dart';

import '../design/image_views.dart';
import '../design/tittle_card.dart';
import 'new_paint_job_dialog.dart';

class PaintJobWidget extends StatelessWidget {
  final PaintJob paintJob;
  final List<PaintJob> allPaintJobs;

  const PaintJobWidget(
      {super.key, required this.paintJob, required this.allPaintJobs});

  @override
  Widget build(BuildContext context) {
    return TittleCard(
      imagePath: paintJob.imagePath,
      title: paintJob.name,
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LayersScreen(
                    paintJob: paintJob,
                  )),
        );
      },
      popUp:
          CustomPopUpMenu(displayOptionals: paintJob.imagePath != null, items: [
        (
          key: "?view",
          value: AppLocalizations.of(context)!.view,
          action: () {
            var jobsWithImage =
                allPaintJobs.where((job) => job.imagePath != null);
            var current = jobsWithImage.indexed
                .firstWhere((mini) => mini.$2.key == paintJob.key);

            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ImageViews(
                      jobsWithImage
                          .map((job) => job.imagePath)
                          .nonNulls
                          .toList(),
                      initialPage: current.$1,
                      name: paintJob.name),
                ));
          }
        ),
        (
          key: "edit",
          value: AppLocalizations.of(context)!.edit,
          action: () {
            NewPaintJobDialog.show(context, null, paintJob);
          }
        ),
        (
          key: "delete",
          value: AppLocalizations.of(context)!.delete,
          action: () {
            paintJob.delete();
          }
        )
      ]),
    );
  }
}
