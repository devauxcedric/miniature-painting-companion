import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:miniature_painting_companion/main.dart';
import 'package:miniature_painting_companion/utils.dart';

import '../Models/hive_models.dart';
import 'history_image_list.dart';
import 'new_paint_job_dialog.dart';
import 'paint_job_widget.dart';

class PaintJobList extends StatefulWidget {
  final Miniature miniature;

  const PaintJobList({super.key, required this.miniature});

  @override
  State<PaintJobList> createState() => _PaintJobList();
}

class _PaintJobList extends State<PaintJobList> {
  bool sortByName = false;

  _PaintJobList();

  List<PaintJob> getSortedContent() {
    var miniature = widget.miniature;
    var toSort = miniature.paintJobs.cast<PaintJob>().toList();
    if (sortByName) {
      toSort.sort((a, b) => a.name.compareTo(b.name));
    }
    return toSort;
  }

  @override
  Widget build(BuildContext context) {
    var miniature = widget.miniature;

    return Scaffold(
      appBar: AppBar(
        title: Text(miniature.name),
        actions: [
          IconButton(
            onPressed: () => setState(() {
              sortByName = !sortByName;
            }),
            icon: sortByName
                ? const Icon(Icons.sort_by_alpha)
                : const Icon(Icons.sort),
          )
        ],
      ),
      body: Column(
        children: [
          MiniatureHistoryList(
            miniature: miniature,
          ),
          const SizedBox(height: 20),
          ValueListenableBuilder(
              valueListenable: paintJobBox.listenable(),
              builder: (context, Box box, _) {
                return Expanded(
                    child: GridView.builder(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 100),
                  itemCount: miniature.paintJobs.length,
                  itemBuilder: (context, listIndex) {
                    return PaintJobWidget(
                      paintJob: getSortedContent().elementAt(listIndex),
                      allPaintJobs: getSortedContent(),
                    );
                  },
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                ));
              })
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => NewPaintJobDialog.show(context, miniature, null),
        tooltip: AppLocalizations.of(context)!.addPaintJob,
        child: Utils.getSvgIconSized(40, 40, "plus"),
      ),
    );
  }
}
